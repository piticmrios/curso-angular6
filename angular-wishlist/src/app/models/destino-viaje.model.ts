
import {v4 as uuid} from 'uuid';

export class DestinoViaje {

selected: boolean;
servicios: string[];
id = uuid();

public votes = 0;

constructor( public nombre: string, public u: string) {
this.servicios = ['gimnasio', 'desayuno'];

}
isSelected(): boolean {
return this.selected;
}
setSelected(d: boolean) {
this.selected = d;
}
voteUp(): any {
this.votes++;
}
voteDown(): any  {
this.votes--;
}
}
