import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';

@Component ({
  selector: 'app-listadestinos',
  templateUrl: './listadestinos.component.html',
  styleUrls: ['./listadestinos.component.css'],
  providers: [DestinosApiClient]
})

export class ListadestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];

  constructor(
    private destinosApiClient: DestinosApiClient,
    private store: Store<AppState>
  ) {
  this.onItemAdded = new EventEmitter();
  this.updates = [];
}

  ngOnInit() {
    this.store.select(state => state.destinos.favorito)
    .subscribe(data => {
      const f = data;
      if (f != null) {
              this.updates.push('Se eligió a ' + f.nombre);
            }
    });
   // store.select(state=> state.destinos.items).subscribe(items=>this.all=items);

  }

  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(d: DestinoViaje) {
    this.destinosApiClient.elegir(d);
  }
/*
  getAll() {

  }*/
}
